package ua.mushynskyi.volodymyr.tommy.com.company;

import ua.mushynskyi.volodymyr.tommy.com.company.details.Engine;
import ua.mushynskyi.volodymyr.tommy.com.company.entities.Person;
import ua.mushynskyi.volodymyr.tommy.com.company.professions.Driver;
import ua.mushynskyi.volodymyr.tommy.com.company.vehicles.Lorry;
import ua.mushynskyi.volodymyr.tommy.com.company.vehicles.SportCar;

public class Main {
    public static void main(String[] args) {

        Lorry lorry = new Lorry("MAN", "Вантажівка","5 тонн","20 тонн");
        Person lorryDriver = new Driver("Максим", 22,"Чоловіча","+380468789135","5 років");
        Engine lorryEngine = new Engine("450 к.с.","MAN");

        SportCar sportCar = new SportCar("Lamborghini","Спорткар", "1,5 тонни","350 км/год");
        Person sportCarDriver = new Driver("Олена",25,"Жіноча","+380495875416","7 років");
        Engine sportCarEngine = new Engine("700 к.с.","Lamborghini");

        System.out.println(lorryDriver.toString() + lorryEngine.toString() + lorry.toString());
        System.out.println(sportCarDriver.toString() + sportCarEngine.toString() + sportCar.toString());
        sportCar.carMoveSet();
    }
}
