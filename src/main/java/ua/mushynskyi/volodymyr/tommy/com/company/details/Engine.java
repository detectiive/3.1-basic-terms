package ua.mushynskyi.volodymyr.tommy.com.company.details;

public class Engine {

    private String power;
    private String producer;

    public Engine(String enginePower, String engineProducer){
        power = enginePower;
        producer = engineProducer;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String toString() {
        return "Потужність: " + power + "\n" + "Виробник двигуна: " + producer + "\n";
    }

}
