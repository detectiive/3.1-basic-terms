package ua.mushynskyi.volodymyr.tommy.com.company.entities;

public class Person {

    private String name;
    private Integer age;
    private String sex;
    private String phoneNumber;

    public Person (String personName, Integer personAge, String personSex, String personPhoneNumber){
        name = personName;
        age = personAge;
        sex = personSex;
        phoneNumber = personPhoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String toString() {
        return "Піб: " + name + "\n" + "Вік: " + age + "\n" + "Стать: " + sex + "\n" + "Номер телефону: " + phoneNumber + "\n";
    }
}

