package ua.mushynskyi.volodymyr.tommy.com.company.professions;

import ua.mushynskyi.volodymyr.tommy.com.company.entities.Person;

public class Driver extends Person {
    private String drivingExperience;

    public Driver(String personName, Integer personAge, String personSex, String personPhoneNumber, String personDrivingExperience) {
        super(personName, personAge, personSex, personPhoneNumber);
        this.drivingExperience = personDrivingExperience;
    }

    public String getDrivingExperience() {
        return drivingExperience;
    }

    public void setDrivingExperience(String drivingExperience) {
        this.drivingExperience = drivingExperience;
    }

    public String toString() {
        return "Піб: " + getName() + "\n" + "Вік: " + getAge() + "\n" + "Стать: " + getSex() + "\n" + "Номер телефону: " + getPhoneNumber() + "\n" + "Стаж водіння: " + getDrivingExperience() + "\n";
    }
}
