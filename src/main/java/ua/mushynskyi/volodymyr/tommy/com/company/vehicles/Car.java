package ua.mushynskyi.volodymyr.tommy.com.company.vehicles;

public class Car {

    private String brand;
    private String carClass;
    private String weight;

    public Car(String carBrand, String carCarClass, String carWeight){
        brand = carBrand;
        carClass = carCarClass;
        weight = carWeight;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCarClass() {
        return carClass;
    }

    public void setCarClass(String carClass) {
        this.carClass = carClass;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Виробник: " + brand + "\n" + "Клас автомобіля: " + carClass + "\n" + "Вага: " + weight + "\n";
    }

    public void start() {
        System.out.println("Поїхали");
    }

    public void stop() {
        System.out.println("Зупиняємося");
    }

    public void turnRight() {
        System.out.println("Поворот направо");
    }

    public void turnLeft() {
        System.out.println("Поворот наліво");
    }
    public void carMoveSet(){
        start();
        stop();
        turnRight();
        turnLeft();
    }
}
