package ua.mushynskyi.volodymyr.tommy.com.company.vehicles;

public class Lorry extends Car {

    private String maxWeight;

    public Lorry(String carBrand, String carCarClass, String carWeight, String carMaxWeight){
        super (carBrand, carCarClass, carWeight);
        this.maxWeight = carMaxWeight;
    }

    public String getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(String maxWeight) {
        this.maxWeight = maxWeight;
    }

    @Override
    public String toString() {
        return "Виробник: " + getBrand() + "\n" + "Клас автомобіля: " + getCarClass() + "\n" + "Вага: " + getWeight() + "\n" + "Вантажопідйомність: " + getMaxWeight() + "\n";
    }
}
