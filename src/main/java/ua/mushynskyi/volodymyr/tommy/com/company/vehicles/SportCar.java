package ua.mushynskyi.volodymyr.tommy.com.company.vehicles;

public class SportCar extends Car {
    private String maxSpeed;
    public SportCar(String carBrand, String carCarClass, String carWeight, String carMaxSpeed){
        super (carBrand, carCarClass, carWeight);
        this.maxSpeed = carMaxSpeed;
    }

    public String getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(String maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Виробник: " + getBrand() + "\n" + "Клас автомобіля: " + getCarClass() + "\n" + "Вага: " + getWeight() + "\n" + "Гранична швидкість: " + getMaxSpeed() + "\n";
    }

}
