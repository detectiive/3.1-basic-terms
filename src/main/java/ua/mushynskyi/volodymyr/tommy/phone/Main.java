package ua.mushynskyi.volodymyr.tommy.phone;

import ua.mushynskyi.volodymyr.tommy.phone.phones.Iphone;
import ua.mushynskyi.volodymyr.tommy.phone.phones.NokiaPhone;
import ua.mushynskyi.volodymyr.tommy.phone.phones.SamsungPhone;

public class Main {

    public static void main(String[] args) {
        Iphone iphone = new Iphone("Iphone","11","64 GB","4 GB","4032 x 3024");
        NokiaPhone nokia = new NokiaPhone("Nokia","XR20","128 GB","6 GB");
        SamsungPhone samsung = new SamsungPhone("Samsung", "S22", "256 GB", "8 GB","4032 x 3024");

        System.out.println(iphone.toString());
        iphone.allFunction();

        System.out.println(nokia.toString());
        nokia.allFunction();

        System.out.println(samsung.toString());
        samsung.allFunction();
    }
}
