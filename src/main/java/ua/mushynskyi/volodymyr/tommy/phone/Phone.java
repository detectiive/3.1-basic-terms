package ua.mushynskyi.volodymyr.tommy.phone;

public abstract class Phone {
    private String name;
    private String model;
    private String storage;
    private String RAM;

    public Phone(String phoneName, String phoneModel, String phoneStorage, String phoneRAM) {
        name = phoneName;
        model = phoneModel;
        storage = phoneStorage;
        RAM = phoneRAM;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getRAM() {
        return RAM;
    }

    public void setRAM(String RAM) {
        this.RAM = RAM;
    }
}
