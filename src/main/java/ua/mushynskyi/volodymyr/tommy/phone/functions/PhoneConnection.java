package ua.mushynskyi.volodymyr.tommy.phone.functions;

public interface PhoneConnection {

    void call();
    void sendMessage();
}
