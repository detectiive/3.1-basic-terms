package ua.mushynskyi.volodymyr.tommy.phone.functions;

public interface PhoneMedia {

    void takePhoto();
    void takeVideo();
}
