package ua.mushynskyi.volodymyr.tommy.phone.phones;

import ua.mushynskyi.volodymyr.tommy.phone.Phone;
import ua.mushynskyi.volodymyr.tommy.phone.functions.PhoneMedia;

public class Iphone extends Phone implements PhoneMedia {

    public String expansion;

    public Iphone(String phoneName, String phoneModel, String phoneStorage, String phoneRAM, String expansion) {
        super(phoneName, phoneModel, phoneStorage, phoneRAM);
        this.expansion = expansion;
    }

    public void setExpansion(String expansion) {
        this.expansion = expansion;
    }

    public String getExpansion() {
        return expansion;
    }

    public void takePhoto() {
        System.out.println("Фотографую на айфон" + "\n");
    }

    public void takeVideo() {
        System.out.println("Знімаю відео на айфон" + "\n");
    }

    public void allFunction() {
        takePhoto();
        takeVideo();
    }

    public String toString() {
        return "Назва телефону: " + getName() + "\n" + "Модель: " + getModel() + "\n" + "Пам'ять: " + getStorage() + "\n" + "ОЗУ: " + getRAM() + "\n" + "Роздільна здатність камери: " + getExpansion() + "\n";
    }
}

