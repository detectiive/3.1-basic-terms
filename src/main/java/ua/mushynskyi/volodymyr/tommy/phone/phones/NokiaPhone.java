package ua.mushynskyi.volodymyr.tommy.phone.phones;

import ua.mushynskyi.volodymyr.tommy.phone.Phone;
import ua.mushynskyi.volodymyr.tommy.phone.functions.PhoneConnection;

public class NokiaPhone extends Phone implements PhoneConnection {

    public NokiaPhone(String phoneName, String phoneModel, String phoneStorage, String phoneRAM) {
        super(phoneName, phoneModel, phoneStorage, phoneRAM);
    }
    public void call() {
        System.out.println("Телефоную з нокії" + "\n");
    }

    public void sendMessage() {
        System.out.println("Надсилаю повідомлення з нокії" + "\n");
    }

    public void allFunction(){
        call();
        sendMessage();
    }

    public String toString() {
        return "Назва телефону: " + getName() + "\n" + "Модель: " + getModel() + "\n" + "Пам'ять: " + getStorage() + "\n" + "ОЗУ: " + getRAM() + "\n";
    }
}
