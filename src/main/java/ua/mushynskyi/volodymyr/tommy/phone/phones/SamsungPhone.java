package ua.mushynskyi.volodymyr.tommy.phone.phones;

import ua.mushynskyi.volodymyr.tommy.phone.Phone;
import ua.mushynskyi.volodymyr.tommy.phone.functions.PhoneConnection;
import ua.mushynskyi.volodymyr.tommy.phone.functions.PhoneMedia;

public class SamsungPhone extends Phone implements PhoneConnection, PhoneMedia {

    public String expansion;

    public SamsungPhone(String phoneName, String phoneModel, String phoneStorage, String phoneRAM, String expansion) {
        super(phoneName,  phoneModel, phoneStorage, phoneRAM);
        this.expansion = expansion;
    }

    public void setExpansion(String expansion) {
        this.expansion = expansion;
    }

    public String getExpansion() {
        return expansion;
    }

    public void call() {
        System.out.println("Телефоную з самсунгу" + "\n");
    }

    public void sendMessage() {
        System.out.println("Надсилаю повідомлення з самсунгу" + "\n");
    }

    public void takePhoto() {
        System.out.println("Фотографую на самсунг" + "\n");
    }

    public void takeVideo() {
        System.out.println("Знімаю відео на самсунг" + "\n");
    }

    public void allFunction() {
        call();
        sendMessage();
        takePhoto();
        takeVideo();
    }

    public String toString() {
        return "Назва телефону: " + getName() + "\n" + "Модель: " + getModel() + "\n" + "Пам'ять: " + getStorage() + "\n" + "ОЗУ: " + getRAM() + "\n" + "Роздільна здатність камери: " + getExpansion() + "\n";
    }
}
